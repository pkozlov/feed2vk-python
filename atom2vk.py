from xml.etree import ElementTree
import logging
import sqlite3
import argparse
from datetime import datetime
from contextlib import closing

import requests
import iso8601
import vk


logging.basicConfig(format='%(asctime)s %(message)s', level=logging.INFO)

parser = argparse.ArgumentParser()
parser.add_argument('feed', type=str, help='feed url')
parser.add_argument('vk_app', type=int, help='vk application id')
parser.add_argument('vk_login', type=str, help='vk login')
parser.add_argument('vk_password', type=str, help='vk password')
parser.add_argument('--db', type=str, help='sqlite database file', default='atom2vk.sqlite')

args = parser.parse_args()

SCHEMA = '''PRAGMA foreign_keys = ON;

CREATE TABLE IF NOT EXISTS feed (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  feed TEXT,
  login TEXT,
  updated TIMESTAMP,
  CONSTRAINT feed_login UNIQUE (feed,login)
);

CREATE TABLE IF NOT EXISTS entry (
  id TEXT,
  feed TEXT REFERENCES feed(id),
  PRIMARY KEY (feed,id)
);
'''


def lazy(func):
    @property
    def wrapper(self):
        name = '__%s' % func.__name__
        try:
            value = getattr(self, name)
        except AttributeError:
            value = func(self)
            setattr(self, name, value)
        return value

    return wrapper


class LazyAPI:
    def __init__(self, app_id, login, password):
        self.app_id = app_id
        self.login = login
        self.password = password

    @lazy
    def get(self):
        return vk.API(self.app_id, self.login, self.password)


with sqlite3.connect(args.db, detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES) as db, closing(
        db.cursor()) as cursor:
    url = args.feed

    feed = requests.get(url)
    root = ElementTree.fromstring(feed.text)
    updated_text = root.findtext('{http://www.w3.org/2005/Atom}updated')
    entries = root.findall('{http://www.w3.org/2005/Atom}entry')

    updated_timestamp = iso8601.parse_date(updated_text).timestamp()
    updated = datetime.utcfromtimestamp(updated_timestamp)

    cursor.executescript(SCHEMA)

    cursor.execute('SELECT id,updated FROM feed WHERE feed=? AND login=?',
                   (url, args.vk_login))

    rs = cursor.fetchone()

    logging.info('Feed was updated %s last time', updated)

    vk_api = LazyAPI(args.vk_app, args.vk_login, args.vk_password)

    if rs:
        logging.info('Synchronizing new entries...')
        (feed_id, feed_updated) = rs
        cursor.execute('SELECT id FROM entry WHERE feed=?', (feed_id,))
        existed = set(map(lambda x: x[0], cursor.fetchall()))

        items = []
        for entry in entries:
            entry_id = entry.findtext('{http://www.w3.org/2005/Atom}id')
            if entry_id not in existed:
                for link in entry.findall('{http://www.w3.org/2005/Atom}link'):
                    if link.attrib.get('rel') == 'alternate':
                        href = link.attrib.get('href')
                        logging.info('Adding href=%s', href)
                        vk_api.get.wall.post(attachments=href)
                        items.append((entry_id, feed_id))
        if items:
            cursor.executemany('INSERT INTO entry VALUES (?,?)', items)
        logging.info('Inserted %d entries', len(items))


    else:
        logging.info('Inserting all entries')
        cursor.execute('INSERT INTO feed (feed, login, updated) VALUES (?,?,?)',
                       (url, args.vk_login, updated))
        feed_id = cursor.lastrowid
        items = [(entry.findtext('{http://www.w3.org/2005/Atom}id'), feed_id) for entry in entries]
        cursor.executemany('INSERT INTO entry (id,feed) VALUES (?,?)', items)
        logging.info('Inserted %d entries', len(items))

